﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerCollision : MonoBehaviour {
    public Text Score;
    public float scale;
    public string tagged;
    public string tags;
    int count;
    
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag==tagged)
        {
            transform.localScale += new Vector3(scale, scale, scale);
            Camera.main.orthographicSize = Camera.main.orthographicSize + 0.05f;
            Destroy(other.gameObject);
            count += 10;
            Score.text = "Score:" + count;
		
        }
		if ((other.gameObject.tag == "Enemy") && this.transform.localScale.x > other.transform.localScale.x)
		{
			Destroy (other.gameObject);
		}
    }
  

}
