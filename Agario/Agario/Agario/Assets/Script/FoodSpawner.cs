﻿using UnityEngine;
using System.Collections;

public class FoodSpawner : MonoBehaviour {

    public GameObject Food;
    public float spawn;
	// Use this for initialization
	void Start () {
        InvokeRepeating("Respawn",0,spawn);
	}
    void Respawn()
    {
        int x = Random.Range(0, Camera.main.pixelWidth);
        int y = Random.Range(0, Camera.main.pixelHeight);
        Vector3 Target = Camera.main.ScreenToWorldPoint(new Vector3(x,y,0));
        Target.z = 0;
        Instantiate(Food, Target, Quaternion.identity);
        

    }
}
