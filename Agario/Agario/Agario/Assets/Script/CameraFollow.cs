﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private float Scale;
	public GameObject MainCamera;
	private Vector3 offset;
	// Use this for initialization
	void Start()
	{
		offset = transform.position - MainCamera.transform.position;
	}
	// Update is called once per frame
	void LateUpdate()
	{
		if(MainCamera!=null)
			transform.position = MainCamera.transform.position + offset;


		{
			if (MainCamera.transform.localScale.x > Scale) 
				Camera.main.orthographicSize += .90f;
			Scale = MainCamera.transform.localScale.x;

			if (MainCamera.transform.localScale.x < Scale) 
				Camera.main.orthographicSize -= .90f;
			Scale = MainCamera.transform.localScale.x;
		}

	}
}
