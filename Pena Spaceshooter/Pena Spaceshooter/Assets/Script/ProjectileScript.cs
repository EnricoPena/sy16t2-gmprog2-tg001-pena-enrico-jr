﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProjectileScript : MonoBehaviour {
    float speed = 20;
    public Text Score;
    private int count;
    Vector3 pos = new Vector3(Random.Range(-5, 5), 7, 0);
    void Update()
    {
        count = 0;
       
        this.transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
    void OnTriggerEnter(Collider other)
    {
        //  Debug.Log(other.gameObject.name);
        if (other.gameObject.name == "Asteriod")
        {
            Destroy(this.gameObject);
            Destroy(other.gameObject);

        }
    }

    void OnCollisionEnter(Collision other)
    {
        other.gameObject.SetActive(false);
        StartCoroutine(Respawn(other.gameObject));
       
    }

    IEnumerator Respawn(GameObject gObj)
    {
        yield return new WaitForSeconds(1);
        gObj.transform.position = pos;
        gObj.SetActive(true);
    }
    
}
