﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {
   
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter(Collision other)
    {
        other.gameObject.SetActive(false);
        StartCoroutine(Respawn(other.gameObject));
    }

    IEnumerator Respawn(GameObject gObj)
    {
        yield return new WaitForSeconds(2);
        gObj.transform.position = new Vector3(Random.Range(-5, 5), 10, 0);
        gObj.SetActive(true);
    }
}
