﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float MoveSpeed = 5.0f;


	// Update is called once per frame
	void Update () 
	{
		float x = Input.GetAxis ("Horizontal");
		float y = Input.GetAxis ("Vertical");

		transform.position += new Vector3 (x, y, 0) * MoveSpeed * Time.deltaTime;
	}
}
