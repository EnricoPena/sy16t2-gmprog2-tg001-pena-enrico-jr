﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerRespawn : MonoBehaviour {
    public GameObject player;
    public Transform playerSpawnPoint;
    public int lives = 3;
    public Text Health;
    private int count;
    // Use this for initialization
    void Start() {
        count = 3;
        SetHealth();
    }

    // Update is called once per frame
    void Update() {

    }
    void OnCollisionEnter(Collision other)
    {

        other.gameObject.SetActive(false);
        StartCoroutine(Respawn(other.gameObject));

        if (other.gameObject.name == "Asteroid")
        {
            Debug.Log(lives);
            lives--;
            count = count - 1;
            SetHealth();
            if (lives <= 0)
            {
                Destroy(this.gameObject);
            }
        }
        if (other.gameObject.name == "Health")
        {
            lives++;
            count = count+1;
            if(lives >=7)
            {
                lives = 7;
            }
            if(count>=7)
            {
                count = 7;
            }
            SetHealth();
        }
    }
    IEnumerator Respawn(GameObject gObj)
    {
        yield return new WaitForSeconds(0);
        gObj.transform.position = new Vector3(Random.Range(-5,5),10,0);
        gObj.SetActive(true);
    }
    void SetHealth()
    {
        Health.text = "Health: " + count.ToString();
    }
}
    
