﻿using UnityEngine;
using System.Collections;

public enum TowerType
{
    Crossbow,
    Cannon,
    Fire,
    Ice
}

public class TowerClass
{
    public GameObject _towerPrefab;
    public int _buildingCost;
    public float _buildingTime;
    public TowerType _type;

    public string _info;

    public TowerClass(GameObject prefab, int cost, TowerType type, float time, string info)
    {
        _towerPrefab = prefab;
        _buildingCost = cost;
        _type = type;
        _buildingTime = time;
        _info = info;
    }
}
