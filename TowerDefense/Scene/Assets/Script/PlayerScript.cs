﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public int gold;
    public int baseCoreLife = 20;
    public int coreLife;

    public Text goldText;
    public Image lifeBar;

    void Start()
    {
        gold = 500;
        coreLife = baseCoreLife;
    }

    void Update()
    {
        goldText.text = gold.ToString();
        lifeBar.fillAmount = (float)coreLife / baseCoreLife;
    }

    void OnTriggerEnter (Collider other)
    {
        // Destroys all the monsters that reach the core
        Destroy(other.gameObject);
        coreLife -= 1;

        if (other.gameObject.GetComponent<MonsterScript>().isBoss)
        {
            coreLife = 0;
        }
    }
}
