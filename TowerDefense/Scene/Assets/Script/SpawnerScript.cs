﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SpawnerScript : MonoBehaviour
{
    // A list of all the waves that going to happen in this game
    public List<WaveClassScript> MonsterWaves = new List<WaveClassScript>();

    // The prefab of all the enemies. These are the only ones I managed to use
    public GameObject goblinPrefab;
    public GameObject horseKnightPrefab;
    public GameObject dragonPrefab;
    public GameObject ravenPrefab;
    public GameObject spiderPrefab;
    public GameObject peasantPrefab;
    public GameObject knight2Prefab;
    public GameObject pigPrefab;
    public GameObject slimePrefab;
    public GameObject knightPrefab;

    public GameObject target; // The end point
    public Material bossMat; // The material of the boss in the minimap
    public int waveCount; // The number of the current wave

    public GameObject textUI; // For inputing some text
    public WaveClassScript currentWave; // The current wave happening

    public List<int> MonsterNumbers = new List<int>();
    public List<GameObject> MonsterPrefabs = new List<GameObject>();
    
    void Start ()
    {
        foreach (WaveClassScript wave in MonsterWaves)
        {
            wave._spawnEnd = false;
        }
        //textUI = transform.Find("Warning").gameObject; // Finds the UI to be inputed with text
        waveCount = 0; 

        // Sets the current wave to the first in the wave list
        currentWave = MonsterWaves[waveCount];

        SetMonsterNumbers();

        MonsterPrefabs.Add(goblinPrefab);
        MonsterPrefabs.Add(horseKnightPrefab);
        MonsterPrefabs.Add(dragonPrefab);
        MonsterPrefabs.Add(ravenPrefab);
        MonsterPrefabs.Add(spiderPrefab);
        MonsterPrefabs.Add(peasantPrefab);
        MonsterPrefabs.Add(knight2Prefab);
        MonsterPrefabs.Add(pigPrefab);
        MonsterPrefabs.Add(slimePrefab);
        MonsterPrefabs.Add(knightPrefab);

        // Start monster spawn
        StartCoroutine(SpawnWave());
    }
	
	void Update ()
    {
	    currentWave._monsters.RemoveAll(obj => obj == null); // Removes from the list all the objects that have been destroyed

        if ((waveCount == MonsterWaves.Count - 1)) return; // if the last wave has been spawned, the code below doesn't execute

        // Moves the current wave into the next if all the monsters of the current wave had been destroyed
        // Space bar to spawn the next wave in advance
        if ((currentWave.isEnd || Input.GetKeyUp(KeyCode.Space)) && currentWave._spawnEnd)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerScript>().gold += 50 * (waveCount + 1);
            textUI.GetComponent<Text>().text = "Next Wave Enters!";
            waveCount++;
            currentWave = MonsterWaves[waveCount];
            SetMonsterNumbers();
            StartCoroutine(SpawnWave());
        }
    }

    IEnumerator SpawnWave()
    {
        if (waveCount <= 0)
        {
            textUI.GetComponent<Text>().text = "10 seconds until the first wave. Build your towers";
            yield return new WaitForSeconds(10.0f);
        }

        textUI.GetComponent<Text>().text = "Spawning wave!";
        for (int i = 0; i < MonsterPrefabs.Count; i++)
        {
            for(int n = 0; n < MonsterNumbers[i]; n++)
            {
                GameObject monster = Instantiate(MonsterPrefabs[i], transform.position, transform.rotation) as GameObject;
                monster.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(target.transform.position);

                monster.GetComponent<MonsterScript>().hpMult = currentWave._hpMult;
                monster.GetComponent<MonsterScript>().goldMult = currentWave._goldMult;
                currentWave._monsters.Add(monster);
                yield return new WaitForSeconds(0.3f);
            }
        }

        // Spawn a random boss monster at the end of the wave if the wave is a boss wave
        if (currentWave._isBossWave)
        {
            int rand = Random.Range(0, MonsterPrefabs.Count - 1);

            GameObject boss = Instantiate(MonsterPrefabs[rand], transform.position, transform.rotation) as GameObject;

            boss.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(target.transform.position);
            boss.GetComponent<MonsterScript>().hpMult = 5.0f;
            boss.GetComponent<MonsterScript>().goldMult = 5.0f;
            boss.GetComponent<MonsterScript>().isBoss = true;

            boss.transform.transform.Find("Cylinder").gameObject.GetComponent<Renderer>().material = bossMat;

            currentWave._monsters.Add(boss);
        }
           
        // All monsters in this wave has been spawned
        currentWave._spawnEnd = true; 
         if (waveCount  < MonsterWaves.Count - 1) textUI.GetComponent<Text>().text = "Current wave spawning over";
    }

    void SetMonsterNumbers()
    {
        MonsterNumbers.Clear();
        MonsterNumbers.Add(currentWave._goble);
        MonsterNumbers.Add(currentWave._horseKnight);
        MonsterNumbers.Add(currentWave._dragon);
        MonsterNumbers.Add(currentWave._raven);
        MonsterNumbers.Add(currentWave._spider);
        MonsterNumbers.Add(currentWave._peasant);
        MonsterNumbers.Add(currentWave._knight2);
        MonsterNumbers.Add(currentWave._pig);
        MonsterNumbers.Add(currentWave._slime);
        MonsterNumbers.Add(currentWave._knight);
    }
}
