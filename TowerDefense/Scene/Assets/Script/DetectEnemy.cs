﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DetectEnemy : MonoBehaviour
{
    List<GameObject> enemies = new List<GameObject>();
    void Update()
    {
        enemies.RemoveAll(obj => obj == null);
        transform.parent.GetComponent<Crossbow>().targets = enemies;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            enemies.Add(other.gameObject);
        }
        if (other.gameObject.tag == "Boss")
        {
            enemies.Add(other.gameObject);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            enemies.Remove(other.gameObject);
        }
        if(other.gameObject.tag=="Boss")
        {
            enemies.Remove(other.gameObject);
        }
    }
}
