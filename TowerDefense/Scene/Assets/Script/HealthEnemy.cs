﻿using UnityEngine;
using System.Collections;

public class HealthEnemy : MonoBehaviour {

    public int health;
    public void TakeDMG(int Value)
    {
        health -= Value;
    }
    void Update()
    {
        if(health<=0)
        {
            Destroy(gameObject);
        }
    }
}
