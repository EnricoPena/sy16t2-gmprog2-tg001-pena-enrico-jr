﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Crystal : MonoBehaviour {
    private UnityEngine.AI.NavMeshAgent agent;
    public GameObject target;
    public int dmg;
    HealthEnemy hpEnemy;
    void Start()
    {
       agent = target.GetComponent<UnityEngine.AI.NavMeshAgent>();   
        
        hpEnemy = GetComponent<HealthEnemy>();
    }
    void Update()
    {
        Rigidbody rigidBody = GetComponent<Rigidbody>(); // get the rigidbody component
        rigidBody.velocity = (target.transform.position - transform.position) * 5.0f; // dir
        if (target == null) Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            agent.speed = 1;
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        if (other.tag == "Boss")
        {

            Destroy(gameObject);
        }
    }
}
