﻿using UnityEngine;
using System.Collections;

public class MonsterScript : MonoBehaviour
{
    // the hp bar that floats above the enemy head;
    public GameObject HpBar;
    float HPBarScale;

    // for difficulty
    public float hpMult;
    public float goldMult;

    // hp and gold
    public int baseGold;
    int gold;
    public int baseHP;
    public int maxHP;
    public int curHP;

    public GameObject icyPrefab;
    public GameObject firePrefab;
    public GameObject player;

    public bool isBoss = false;
    bool damageOverTime = false;

    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        gold = (int)(baseGold * goldMult);
        maxHP = (int)(baseHP * hpMult);

        curHP = maxHP;
        HPBarScale = HpBar.transform.localScale.x;
    }

	void Update ()
    {
        float rate = (float)curHP / maxHP;
        HpBar.transform.localScale = new Vector3(HPBarScale * rate, HpBar.transform.localScale.y, HpBar.transform.localScale.z);

        if (curHP <= 0.0f)
        {
            Destroy(gameObject);
            player.GetComponent<PlayerScript>().gold += getGold();
        }
    }

    public void takeDamage(float damage)
    {
        curHP -= (int)damage;
    }

    public int getGold()
    {
        return gold;
    }

    public void ApplySlow(float slowMult)
    {
        UnityEngine.AI.NavMeshAgent navMesh = GetComponent<UnityEngine.AI.NavMeshAgent>();

        StartCoroutine(Slow(navMesh, slowMult));

    }

    public void ApplyDamageOverTime(float DOTMult)
    {
        damageOverTime = true;
        StartCoroutine(DamageOverTime(DOTMult));
    }

    IEnumerator Slow(UnityEngine.AI.NavMeshAgent navMesh, float slowMult)
    {
        Debug.Log("Apply Slow!");
        GameObject icy = Instantiate(icyPrefab, transform.position, Quaternion.identity) as GameObject;
        icy.transform.SetParent(transform);

        navMesh.speed -= slowMult;
        if (navMesh.speed <= 1) navMesh.speed = 1.0f;
        yield return new WaitForSeconds(2.0f * slowMult);

        navMesh.speed = 5.0f;
        Destroy(icy);
        Debug.Log("Speed Normal!");
    }

    IEnumerator DamageOverTime(float damageMult)
    {
        Debug.Log("DOT start!");
        GameObject fire = Instantiate(firePrefab, transform.position, Quaternion.identity) as GameObject;
        fire.transform.SetParent(transform);
        StartCoroutine(DamagePerSecond(damageMult));
        yield return new WaitForSeconds(5.0f * damageMult);

        damageOverTime = false;
        Destroy(fire);
        Debug.Log("DOT end!");
    }

    IEnumerator DamagePerSecond(float damageMult)
    {
        if (damageOverTime)
        {
            takeDamage(5.0f * damageMult);
            yield return new WaitForSeconds(1.0f);
            yield return StartCoroutine(DamagePerSecond(damageMult));
        }
    }
}
