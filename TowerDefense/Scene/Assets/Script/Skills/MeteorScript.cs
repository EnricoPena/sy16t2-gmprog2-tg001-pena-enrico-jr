﻿using UnityEngine;
using System.Collections;

public class MeteorScript : MonoBehaviour
{
    public GameObject explosion;
    // Use this for initialization
    void Start ()
    {

    }

    // Update is called once per frame
    void Update ()
    {

    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            GameObject explode = Instantiate(explosion, transform.position, Quaternion.identity) as GameObject;
            Destroy(gameObject);
        }
    }
}
