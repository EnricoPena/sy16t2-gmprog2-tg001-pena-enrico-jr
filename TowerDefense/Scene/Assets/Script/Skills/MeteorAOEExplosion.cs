﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeteorAOEExplosion : MonoBehaviour
{
    public List<GameObject> targets = new List<GameObject>();
    
    bool hit = false;

    void Start ()
    {

    }

    void Update ()
    {
        Destroy(gameObject, 0.3f);

        if (!hit && targets.Count != 0)
        {
            hit = true;
        }
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            targets.Add(other.gameObject);

        }

    }
}
