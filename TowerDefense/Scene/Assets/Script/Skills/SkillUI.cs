﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class SkillUI : MonoBehaviour
{
    public bool isMeteor;
    public bool meteorCooldown;
    public bool apocalypseCooldown;
    public bool reaperCooldown;

    public GameObject meteorPrefab;
    public GameObject meteorExplosion;
    public GameObject particlesEffect;

    public Button meteorButton;
    public Button apocalypseButton;
    public Button reaperButton;
    
	
	void Start ()
    {
        apocalypseCooldown = false;
        meteorCooldown = false;
        reaperCooldown = false;
        isMeteor = false;
	}
	
	void Update ()
    {
        if (isMeteor) meteorButton.gameObject.GetComponent<Image>().color = Color.grey;
        else meteorButton.gameObject.GetComponent<Image>().color = Color.white;

        if (Input.GetMouseButtonDown(0) && isMeteor && EventSystem.current.IsPointerOverGameObject() == false)
        {
            Vector3 position = SelectObjectbyMousePos();

            if (position != Vector3.zero)
            {
                MeteorSkill(position);
                isMeteor = false;
            }
        }

        if (meteorCooldown)
        {
            meteorButton.interactable = false;
            meteorButton.image.fillAmount += Time.deltaTime / 10;
            if (meteorButton.image.fillAmount == 1.0f) meteorCooldown = false;
        }
        if (!meteorCooldown) meteorButton.interactable = true;

        if (apocalypseCooldown)
        {
            apocalypseButton.interactable = false;
            apocalypseButton.image.fillAmount += Time.deltaTime / 40;
            if (apocalypseButton.image.fillAmount == 1.0f) apocalypseCooldown = false;
        }
        if (!apocalypseCooldown) apocalypseButton.interactable = true;

        if (reaperCooldown)
        {
            reaperButton.interactable = false;
            reaperButton.image.fillAmount += Time.deltaTime / 5;
            if (reaperButton.image.fillAmount == 1.0f) reaperCooldown = false;
        }

       
    }

    public void Meteor()
    {
        isMeteor = !isMeteor;
    }

    public void Apocalypse()
    {
        StartCoroutine(ApocalypseSkill());
    }

    Vector3 SelectObjectbyMousePos ()
    {
        Vector3 selected = Vector3.zero;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(ray);

        foreach (RaycastHit hit in hits)
        {
            {
                selected = hit.point;
            }
        }

        return selected;
    }

    void MeteorSkill(Vector3 point)
    {
        GameObject meteor = Instantiate(meteorPrefab, new Vector3(point.x + 3.0f, point.y + 5.0f, point.z + 1.0f), Quaternion.identity) as GameObject;
        meteor.GetComponent<Rigidbody>().velocity = (point - meteor.transform.position).normalized * 30.0f;

        meteorCooldown = true;
        meteorButton.image.fillAmount = 0.0f;
    }

    IEnumerator ApocalypseSkill()
    {
        List<GameObject> monsters = new List<GameObject>();
        monsters.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        monsters.RemoveAll(obj => obj == null);

        foreach (GameObject monster in monsters)
        {
            GameObject particle = Instantiate(particlesEffect, monster.transform.position, transform.rotation) as GameObject;
            particle.transform.parent = monster.transform;
        }
        apocalypseCooldown = true;
        apocalypseButton.image.fillAmount = 0.0f;
        yield return new WaitForSeconds(1.0f);
		foreach (GameObject monster in monsters) 
		{
			Destroy (monster);
		}
    }

   
}
