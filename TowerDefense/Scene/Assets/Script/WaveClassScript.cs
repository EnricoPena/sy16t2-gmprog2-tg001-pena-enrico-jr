﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaveClassScript : ScriptableObject
{
    // List of the monster gameobject in the current wave. Added after instantiated
    public List<GameObject> _monsters = new List<GameObject>();

    // Number of each monster contained in this wave
    
    public int _slime, _spider, _raven, _pig, _goble, _peasant, _knight, _knight2, _horseKnight, _dragon;

    // Hp and gold multiplier. Increases each wave pass
    public float _hpMult, _goldMult;

    public bool _isBossWave; // Does this wave have a boss? Spawns a random monster at the end of the wave with 5x more hp and gold
    bool _isEnd;            // Returns true if all the monsters in this wave are either dead or have reached the core
    public bool _spawnEnd = false; // Returns true if this wave has finished spawning its monsters

    public bool isEnd { get { return _monsters.Count <= 0; } }

    // Class constructor
    public WaveClassScript(bool isBoss, float HpMultiplier, float goldMultiplier, int gobleNum, int horseKnightNum, int dragonNum, int ravenNum, int spiderNum, int peasantNum, int knight2Num, int pigNum, int slimeNum, int knightNum)
    {
        _hpMult = HpMultiplier;
        _goldMult = goldMultiplier;

        _goble = gobleNum;
        _horseKnight = horseKnightNum;
        _dragon = dragonNum;
        _raven = ravenNum;
        _spider = spiderNum;
        _peasant = peasantNum;
        _knight2 = knight2Num;
        _pig = pigNum;
        _slime = slimeNum;
        _knight = knightNum;

        _isBossWave = isBoss;
        _isEnd = false;
        _spawnEnd = false;
        
        _monsters = new List<GameObject>();
    }
}
