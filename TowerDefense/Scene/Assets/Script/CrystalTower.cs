﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CrystalTower : MonoBehaviour {

    public GameObject Crystal;
    public List<GameObject> targets = new List<GameObject>();
    void Start()
    {
        StartCoroutine(Attack());
    }
    void Update()
    {
        targets.RemoveAll(obj => obj == null);
        if (targets.Count == 0) return;
    }
    IEnumerator Attack()
    {
        if (targets.Count != 0)
        {
            GameObject target = targets[0];
            Quaternion rotation = Quaternion.LookRotation(target.transform.position);
            GameObject projectile = Instantiate(Crystal, transform.position, rotation) as GameObject;
            projectile.GetComponent<arrow>().target = target;
        }
        yield return new WaitForSeconds(1.0f);
        yield return StartCoroutine(Attack());
    }
}
