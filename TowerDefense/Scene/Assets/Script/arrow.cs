﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class arrow : MonoBehaviour
{
   
    public GameObject target;
    public int dmg;
    HealthEnemy hpEnemy;
    void Start()
    { 
        hpEnemy = GetComponent<HealthEnemy>();
    }
    void Update()
    {
        Rigidbody rigidBody = GetComponent<Rigidbody>(); // get the rigidbody component

        if (target != null)
        {
            rigidBody.velocity = (target.transform.position - transform.position) * 5.0f; // dir
        }
        else { Destroy(gameObject); }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
        if (other.tag == "Boss")
        {
            
            Destroy(gameObject);
        }
    }
}
