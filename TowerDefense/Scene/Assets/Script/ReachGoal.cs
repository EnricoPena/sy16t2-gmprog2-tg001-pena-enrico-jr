﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ReachGoal : MonoBehaviour {
    
    public Text hp;
    public float health=100;
    public int value;
	void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Enemy")
        {
            Destroy(other.gameObject);
            health -=value;
        }
        if (other.gameObject.tag == "Boss")
        {
            health = 0;
        }
        if (health != 0)
        {
            hp.text = " " + health;
        }
        else
        {
            hp.text = " " + 0;
        }
       
    }
    
}
