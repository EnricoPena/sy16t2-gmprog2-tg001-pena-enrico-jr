﻿using UnityEngine;
using System.Collections;

public class ArrowScript : MonoBehaviour
{
    public float damageMult;
    public GameObject target;

    Rigidbody arrowRigidBody;
    
    void Start ()
    {
        arrowRigidBody = GetComponent<Rigidbody>();
    }
	
	void Update ()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.transform.position - transform.position;

        var rot = Quaternion.LookRotation(dir); //Rotate the tower towards the desired target

        transform.rotation = Quaternion.Slerp(transform.rotation, rot, 1.0f * Time.deltaTime);
        arrowRigidBody.velocity = (target.transform.position - transform.position).normalized * 50.0f;
        arrowRigidBody.velocity = new Vector3(arrowRigidBody.velocity.x, arrowRigidBody.velocity.y + 10.0f, arrowRigidBody.velocity.z);
    }

    public float Damage()
    {
        return ((Random.Range(30, 50)) * damageMult);
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "MonsterGround" || other.gameObject.tag == "MonsterFlying")
        {
            other.GetComponent<MonsterScript>().takeDamage(Damage());
            Destroy(gameObject);
        }
    }
}
