﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public GameObject target;
    public int dmg;
    HealthEnemy hpEnemy;
    void Start()
    {
        hpEnemy = GetComponent<HealthEnemy>();
    }
    void Update()
    {
        Rigidbody rigidBody = GetComponent<Rigidbody>(); // get the rigidbody component
        
        if (target != null)
        {
            rigidBody.velocity = (target.transform.position - transform.position) * 5.0f; // dir
        }
        else { Destroy(gameObject); }
          
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            // Debug.Log("Enemy is hit!");
            //HPEnemy.TakeDMG(ArrowDMG);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }
}
